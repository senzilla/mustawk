#!@SHEBANGDIR@/awk -f
# SPDX-License-Identifier: ISC
# vi: syntax=awk

# TODO: Simplify cond()
function cond(a, n, s) { b = val(n, s) ? 1 : 0; C = (a == b) ? 0 : TPL[n + 1]; return !C }
function trim(s) { sub(/^[[:space:]]*/, "", s); sub(/[[:space:]]*$/, "", s); return s }
function unescape(s) { gsub(/\\n/, "\n", s); return s }

function loop(n, s, o, t, nn)
{
	for (o = 1; o <= O[s, TPL[n + 1]]; o++)
	{
		t = t TPL[n + 4]
		for (nn = n + 5; nn < length(TPL); nn = nn + 5)
		{
			if (TPL[nn] == "/" && TPL[nn + 1] == TPL[n + 1]) break
			N = nn
			t = t tpl(nn, V[s, TPL[n + 1], o])
		}
	}
	return t
}

function partial(f, i, s, l)
{
	for (i = 0; i <= 5; i++)
	{
		if (system("test -f " f) == 0) break # Partial found
		if (i == 5) return # No partial found
		f = "../" f
	}
	while (getline l < f) s = s ? s ORS l : l
	return s
}

# TODO: Exit with error for incorrectly formatted tags
function tag(s, z)
{
	delete TAG
	split(s, TAG, ":")
	z = substr(s, 0, 1)
	TAG[0] = (z == "#" || z == "/" || z == "^" || z == ">") ? z : 0
	if (TAG[0]) TAG[1] = substr(s, 2) # XXX: Tag name without qualifier
	TAG[1] = tolower(TAG[1])
	return (length(TAG) > 4 || TAG[1] ~ /[^a-z0-9_:\-\/\.]/) ? 0 : 1
}

function tpl(n, s)
{
	if (TPL[n] == "/" && C == TPL[n + 1]) C = 0
	if (C) return
	if (TPL[n] == 0) return val(n, s) TPL[n + 4]
	if (TPL[n] == ">") return partial(TPL[n + 1] ".mustache") TPL[n + 4]
	if (TPL[n] == "#" && L[V[s, TPL[n + 1], 1]]) return loop(n, s)
	if (TPL[n] == "#" && !cond(1, n, s)) return
	if (TPL[n] == "^" && !cond(0, n, s)) return
	return TPL[n + 4]
}

function val(n, s, k, v, a, i)
{
	k = index(TPL[n + 1], "_") == 1 ? TPL[n + 1] : s SUBSEP TPL[n + 1]
	if (TPL[n + 3]) return substr(V[k], TPL[n + 2], TPL[n + 3])
	v = V[k]
	if (index(TPL[n + 2], "E")) v = substr(v, index(v, ":") + 1)
	if (index(TPL[n + 2], "R")) v = substr(v, 0, index(v, ":") - 1)
	if (index(TPL[n + 2], "L")) v = tolower(v)
	if (index(TPL[n + 2], "l")) v = tolower(substr(v, 0, 1)) substr(v, 2)
	if (index(TPL[n + 2], "U")) v = toupper(v)
	if (index(TPL[n + 2], "u")) v = toupper(substr(v, 0, 1)) substr(v, 2)
	if (index(TPL[n + 2], "X") && match(v, /\.[1-9]$/))
		v = substr(v, 0, RSTART-1) "(" substr(v, RSTART+1, 1) ")"
	if (index(TPL[n + 2], "W") && split(v, a, " "))
	{
		v = ""
		# TODO: Consider existing newlines and reset counter.
		for (i = 1; i <= length(a); i++)
			v = (i % 12 == 0) ? v a[i] "\n" : v a[i] " "
	}
	if (index(TPL[n + 2], "N") && v) v = v "\n"
	return v
}

BEGIN {
	ORS = ""
	SUBSEP = "."
	HEAD = C = 0
	V["_base"] = ENVIRON["BASE"] ? ENVIRON["BASE"] : "http://localhost:8080"
}

END {
	if (ENVIRON["HEAD"]) print "---\n" H "\n---\n"
	l = ENVIRON["LIST"] ? length(L) : 1
	for (N = A[1]; N < B[1]; N = N + 5) V["_main"] = V["_main"] tpl(N)
	if (l > 0)
	{
		for (s = 1; s <= l; s++)
			if (!ENVIRON["FILTER"] || match(S[s], ENVIRON["FILTER"]))
				for (N = A[2]; N < B[2]; N = N + 5) print tpl(N, S[s])
	}
	else
		for (N = A[2]; N < B[2]; N = N + 5) print tpl(N)
}

FNR == 1 { F++; A[F] = B[F] = length(TPL) }
/^---$/ {
	HEAD = !HEAD
	if (!HEAD)
	{
		RS = "{{"
		FS = "}}\n?"
	}
	next
}

HEAD {
	if (ENVIRON["LIST"]) H = H ? H "\n" $0 : $0
	s = $1
	p = $2
	$1 = $2 = $3 = $4 = "" # XXX: Handle language in $4
	L[s] = 1
	O[s, p] ? O[s, p]++ : O[s, p] = 1
	S[length(L)] = V[s, "."] = s
	V[s, p] = V[s, p, O[s, p]] = trim(unescape($0))
	# XXX: Provide @typeof as a boolean variable
	if (p == "a")
	{
		a = V[s, p]
		gsub(":", ".", a) # XXX: Same conversion as triple-text(1)
		V[s, a] = 1
	}
}

!HEAD && /^!/ { $1 = "" }

!HEAD && tag($1) {
	for (n = 0; n < 4; n++) TPL[B[F]++] = TAG[n]
	TPL[B[F]++] = $2 ? $2 : ""
	next
}

!HEAD { for (n = 0; n < 4; n++) TPL[B[F]++] = ""; TPL[B[F]++] = $1 }

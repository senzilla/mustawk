mustawk - mustache implementation in awk
========================================

Subset of [mustache(5)](http://mustache.github.io/mustache.5.html)
implemented in awk. Works great for simple websites, etc.
